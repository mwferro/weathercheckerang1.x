(function() {
  'use strict';

  angular
    .module('app.dashboard')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    var otherwise = '/about';
    routerHelper.configureStates(getStates(), otherwise);
  }

  function getStates() {
    return [{
        state: 'dashboard',
        config: {
          url: '/',
          templateUrl: 'app/dashboard/dashboard.html',
          controller: 'DashboardController',
          controllerAs: 'vm',
          title: 'dashboard',
          settings: {
            nav: 1,
            content: '<i class="fa fa-dashboard"></i> Dashboard'
          },
        }
      }, {
        state: 'dashboard.about',
        config: {
          url: 'about',
          title: 'About',
          templateUrl: 'app/dashboard/about.tpl.html'
        }
      }, {
        state: 'dashboard.tutorial',
        config: {
          url: 'tutorial',
          title: 'tutorial',
          templateUrl: 'app/dashboard/tutorial.tpl.html'
        }
      }, {
        state: 'dashboard.gallery',
        config: {
          url: 'gallery',
          title: 'gallery',
          templateUrl: 'app/gallery/gallery.html'
        }
      }

    ];
  }
})();
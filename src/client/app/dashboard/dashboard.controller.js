(function() {
  'use strict';

  angular.module('app.dashboard')
    .controller('DashboardController', DashboardController);

  DashboardController.$inject = ['$q', '$state'];
  /* @ngInject */
  function DashboardController($q, $state) {
    var vm = this;
    vm.now = new Date();
    vm.title = 'Dashboard';
    vm.touchHome = true;

    vm.showSplash = function() {
      if ($state.current.name == 'dashboard') {
        return true;
      } else {
        return false;
      }
      return $state;
    }

    vm.getState = function() {
      return $state.current.name;
    }

    vm.groups = [{
      title: 'Get a five-day forecast',
      content: "Visit your weather's forcast page by selecting the Forecast button above."
    }, {
      title: 'Get a detailed view of the weather for a time and date segment.',
      content: 'Click on a time and date segment to view detailed information ' +
        'pertaining to your particular selection.'
    }, {
      title: 'Select another city from the dropdown.',
      content: 'Choose to select another city from the dropdown list of ' +
      'popular cities throughout the globe.'
    }];
  }
})();
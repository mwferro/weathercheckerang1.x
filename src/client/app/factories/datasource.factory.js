(function() {
  angular.module('app.factories')
  .factory('DataService', dataservice);

  dataservice.$inject = ['$http', 'OWM_URL', 'IMAGE_LOCATION', 'exception', 'logger'];

  /* @ngInject */
  function dataservice($http, OWM_URL, IMAGE_LOCATION, exception, logger) {
    return {
      getWeather: getWeather
    };

    function getWeather(query) {
      var url = OWM_URL +
      '?q=' + query.name + ',' + query.country + '&type=accurate&units=metric&APPID=04927def95121c234a8c2b5adefd041e';

      console.log('url is: ' + url);

      return $http({
        method: 'GET',
        url: url,
        dataType: 'json'
      })
      .then(getWeatherComplete)
      .catch(getWeatherFailed);

      function getWeatherComplete(response) {
        return response;
      }

      function getWeatherFailed(e) {
        return exception.catcher('XHR Failed for getObjectDetails')(e);
      }
    }
  }
})();
(function() {
  angular.module('app.factories')
    .factory('StaticImporter', staticImporter);

  staticImporter.$inject = ['$http'];

  /* @ngInject */
  function staticImporter($http) {

    return {
      getLocalJSON: getLocalJSON
    };

    function getLocalJSON(target) {
      return $http.get(target)
        .then(getDataComplete)
        .catch(getDataFailed);

      function getDataComplete(response) {
        return response;
      }

      function getDataFailed(e) {
        return exception.catcher('XHR Failed for StaticImporter::staticImporter')(e);
      }
    }
  }
})();
(function() {
  'use strict';

  angular
    .module('app.layout')
    .controller('ShellController', ShellController);

  ShellController.$inject = ['$rootScope', '$timeout', 'config', 'logger'];
  /* @ngInject */
  function ShellController($rootScope, $timeout, config, logger) {
    var vm = this;
    vm.busyMessage = 'Please wait ...';
    vm.isBusy = true;

    vm.navline = {
      title: config.appTitle,
      text: '&copy; ' + new Date().getFullYear() +  ' Michael Ferro',
      link: '#'
    };

    function activate() {
      logger.success(config.appTitle + ' loaded!', null);
    }

    activate();
  }
})();

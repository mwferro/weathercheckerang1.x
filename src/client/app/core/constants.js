(function() {
  'use strict';
  angular
    .module('app.core')
    .constant('IMAGE_LOCATION', 'http://openweathermap.org/img/w/')
    .constant('OWM_URL', 'http://api.openweathermap.org/data/2.5/forecast')
})();
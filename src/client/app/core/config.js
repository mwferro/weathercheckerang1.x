(function() {
  'use strict';

  var config = {
    appErrorPrefix: '[Weather App Error] ',
    appTitle: 'Your Weather'
  };

  angular.module('app.core')

  .value('config', config)

  .config(configure);

  // configure.$inject = ['$logProvider', 'routerHelperProvider', 'exceptionHandlerProvider'];
  configure.$inject = ['$logProvider', 'exceptionHandlerProvider'];

  /* @ngInject */
  // function configure($logProvider, routerHelperProvider, exceptionHandlerProvider) {
  function configure($logProvider, exceptionHandlerProvider) {
    if ($logProvider.debugEnabled) {
      $logProvider.debugEnabled(true);
    }
    exceptionHandlerProvider.configure(config.appErrorPrefix);
    // routerHelperProvider.configure({
    //   docTitle: config.appTitle + ': '
    // });
  }

})();

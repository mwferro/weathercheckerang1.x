(function() {
  'use strict';

  angular.module('app.gallery')
    .controller('GalleryController', GalleryController);

  GalleryController.$inject =
    ['$uibModal', 'IMAGE_LOCATION', 'StaticImporter', 'moment', '$log', '$state', '$http', 'DataService'
    ];

  function GalleryController($uibModal,  IMAGE_LOCATION, StaticImporter,  moment, $log, $state, $http, DataService) {
    var gc = this;
    var query = null;



    activate();

    function activate() {
      query = (!gc.data) ? { "name": "London", "country": "uk" } : query;

      return DataService.getWeather(query)
        .then(function(resp) {
          var data = resp;
          gc.data = resp;
          return resp;
        })
        .catch(function(err) {
          console.log(err);
        })
        .then(function(data) {
          if (!gc.cities) {
            getCities();
          }
        })
        .catch(function(err) {
          console.log(err);
          logErrors(err);
        });
    }

    function getCities() {
      return StaticImporter.getLocalJSON('app/resources/cities.json')
      .then(function(resp) {
        gc.cities = resp.data.cities;

        return resp;
      })
      .catch(function(err) {
        console.log(err);
      })

    }

    gc.setCityDetails = function(city) {
      gc.resultsDetails = null;

      query = city;
      activate();
    }

    gc.handleDateTimeDetails = function(details) {
      if (isEmpty(details)) {
        gc.resultsDetails = null;
      } else {
        gc.resultsDetails = details;
      }
    }

    gc.getImage = function(details) {
      var imageLocation = IMAGE_LOCATION + details[0].icon + ".png";

      return imageLocation;
    }

    function isEmpty(object) {
      for (var key in object) {
        if (object.hasOwnProperty(key)) {
          return false;
        }
      }
      return true;
    }

    function logErrors(err) {
      console.log('An error occurred: ' + err);
    }
  }

})();
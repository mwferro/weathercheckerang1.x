(function() {
  angular.module('app.directives')
  .directive('dropdown', dropdown);

  function dropdown() {
    'use strict';
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'app/gallery/dropdown.tmpl.html',
      scope: {
       selections: '=',
       callback: '&'
     },
     link: function ($scope, element, attrs) {
      $scope.$watch('selected', function (value) {
        if(value){
          $scope.callback()(value);
        }
      });
    }
  };
}
})();
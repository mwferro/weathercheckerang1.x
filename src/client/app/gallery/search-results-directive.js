  (function() {
    angular.module('app.directives')
      .directive('artifactResultDetails', artifactResultDetails);

    function artifactResultDetails() {
      'use strict';
      return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/gallery/search-results.tmpl.html',
        scope: {
          resultsDetails: '=',
        },
      };
    }
  })();
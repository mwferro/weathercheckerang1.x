(function() {
  'use strict';

  angular.module('app', [
    'app.core',
    'app.widgets',
    'app.gallery',
    'app.dashboard',
    'app.services',
    'app.factories',
    'app.directives',
    'app.layout'
  ]);

  angular.module('app.services', []);
  angular.module('app.factories', []);
  angular.module('app.directives', []);
})();
